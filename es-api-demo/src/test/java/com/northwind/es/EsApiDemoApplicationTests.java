package com.northwind.es;

import com.alibaba.fastjson.JSON;
import com.northwind.es.pojo.User;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.document.DocumentField;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * es测试
 *
 * @author jiajia
 * @date 2020/12/28
 */
@SpringBootTest
class EsApiDemoApplicationTests {

    //region 初始化参数

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient restHighLevelClient;

    //endregion

    //region 索引相关

    /**
     * 创建索引
     * 索引相关命令
     * client.indices().xxx();
     *
     * @throws IOException
     */
    @Test
    void testCreateIndex() throws IOException {
        //1. 创建索引请求
        CreateIndexRequest request = new CreateIndexRequest("kuang_index");
        //2. 执行请求
        CreateIndexResponse createIndexResponse =
                restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);

        //3. 获取回执
        System.out.println(createIndexResponse.index());
        System.out.println(createIndexResponse);
    }

    /**
     * 获取索引，判断指定索引是否存在
     */
    @Test
    void testGetIndex() throws IOException {
        //获取索引
        GetIndexRequest request = new GetIndexRequest("kuang_index");
        //判断索引是否存在
        boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);

        System.out.println(exists);
    }

    /**
     * 删除索引
     */
    @Test
    void testDeleteIndex() throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest("kuang_index");
        AcknowledgedResponse delete = restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);

        System.out.println(delete.toString());
        System.out.println(delete.isAcknowledged());
    }

    //endregion

    //region 文档相关

    /**
     * 创建文档
     */
    @Test
    void testAddDocument() throws IOException {
        // 创建对象
        User user = new User("狂神", 3);
        // 创建请求，获取索引
        IndexRequest indexRequest = new IndexRequest("kuang_index");
        // 规则 put /kuang_index/_doc/1
        // 设置索引
        indexRequest.id("1");
        // 设置超时时间
        indexRequest.timeout(TimeValue.timeValueSeconds(3L));

        // 将数据放入请求 json
        /**
         * 这两种toJson产生的结果不一样，
         *  JSONObject.toJSON(user)会把内容当做字符串处理，即所以字符作为key，没有value
         *  同时  XContentType.JSON 第二个参数也是必传的，不然没有转换的类型，文档无法成功创建
         */
//        indexRequest.source(JSONObject.toJSON(user), XContentType.JSON);
        indexRequest.source(JSON.toJSONString(user), XContentType.JSON);

        // 客户端发送请求,获取请求结果
        IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);

        System.out.println(indexResponse.toString());
        System.out.println(indexResponse.getResult());
        // 对应命令返回的状态 CREATED
        System.out.println(indexResponse.status());
    }

    /**
     * 获取文档，判断是否存在 get
     *
     * @throws IOException
     */
    @Test
    void testIsDocumentExit() throws IOException {

        GetRequest request = new GetRequest("kuang_index", "1");
        // 不获取返回的 _source上下文
        request.fetchSourceContext(new FetchSourceContext(false));
        request.storedFields("_none_");

        boolean exists = restHighLevelClient.exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    /**
     * 获取文档的信息
     * {"age":3,"name":"狂神"}
     * {"_index":"kuang_index","_type":"_doc","_id":"1","_version":1,"_seq_no":0,"_primary_term":1,"found":true,"_source":{"age":3,"name":"狂神"}}
     *
     * @throws IOException
     */
    @Test
    void testGetDocument() throws IOException {

        GetRequest request = new GetRequest("kuang_index", "1");

        GetResponse getResponse = restHighLevelClient.get(request, RequestOptions.DEFAULT);

        System.out.println(getResponse.getSourceAsString());

        Map<String, Object> source = getResponse.getSource();

        getResponse.isExists();

        Map<String, DocumentField> fields = getResponse.getFields();

        System.out.println(getResponse);
    }

    /**
     * 更新文档信息
     *
     * @throws IOException
     */
    @Test
    void testUpdateDocument() throws IOException {

        UpdateRequest updateRequest = new UpdateRequest("kuang_index", "1");
        updateRequest.timeout(TimeValue.timeValueSeconds(3L));

        final User user = new User("狂神说java", 4);

        updateRequest.doc(JSON.toJSONString(user), XContentType.JSON);

        final UpdateResponse update = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println(update.status().toString());
    }

    /**
     * 删除文档信息
     *
     * @throws IOException
     */
    @Test
    void testDeleteDocument() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest("kuang_index", "1");

        final DeleteResponse delete = restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);

        System.out.println(delete.status().toString());
    }

    /**
     * 批量新增数据 Bulk
     *
     * @throws IOException
     */
    @Test
    void testBranchInsert() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
//        BulkRequest bulkRequest = new BulkRequest("kuang_index");
        bulkRequest.timeout(TimeValue.timeValueSeconds(3L));

        final User user = new User("张3", 1);
        final User user1 = new User("李4", 1);
        final User user2 = new User("王5", 1);
        final User user3 = new User("赵6", 1);
        final User user4 = new User("文7", 1);
        final User user5 = new User("丑8", 1);

        List<User> list = new ArrayList<>();
        list.add(user);
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        //批处理请求
        for (int i = 0; i < list.size(); i++) {
            // 批量更新和批量删除等操作，在这里操作就可以了
            bulkRequest.add(
                    new IndexRequest("kuang_shen")
                            .source(JSON.toJSONString(list.get(i)), XContentType.JSON)
            );
        }

        final BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

        // status 状态  hasFailures 是否失败， false, 即成功
        System.out.println(bulk.status() + "  " + bulk.hasFailures());
        System.out.println(bulk.toString());
    }

    /**
     * 查询文档
     * SearchRequest 搜索请求
     * SearchSourceBuilder 条件构造
     * HighLightBuilder 构建高亮
     * TermQueryBuilder 精确查询
     * MatchQueryBuilder
     * xxxQueryBuilder
     */
    @Test
    void testSearch() throws IOException {

        final SearchRequest searchRequest = new SearchRequest("kuangshen");

        // 构建搜索条件
        final SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        // 高亮
        sourceBuilder.highlighter();

        // 精确 这里如果中英文混合，会查询不到结果, 在参数上，加上.keyword即可
        final TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "赵");
//        final TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name.keyword", "赵6");
//
//        sourceBuilder.postFilter(termQueryBuilder);
//
//        sourceBuilder.query(termQueryBuilder);

        // 匹配所有
//        final MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();

        sourceBuilder.query(termQueryBuilder);

        // 一种新的方式
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        // 分页
//        sourceBuilder.from();
//        sourceBuilder.size();

        searchRequest.source(sourceBuilder);

        final SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        System.out.println(searchResponse.toString());
        System.out.println(searchResponse.getHits().toString());
        System.out.println(JSON.toJSONString(searchResponse.getHits()));
        System.out.println(searchResponse.getHits().getMaxScore());
        System.out.println(searchResponse.getHits().getSortFields());

        for (SearchHit hit : searchResponse.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

    }

    //endregion

    @Test
    void contextLoads() {

    }

}
