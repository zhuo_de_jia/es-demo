package com.northwind.es.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * user对象
 *
 * @author jiajia
 * @date 2020/12/28
 */
public class User {
    private String name;
    private int age;
}
