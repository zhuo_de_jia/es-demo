package com.northwind.jd.demo.service;

import com.northwind.jd.demo.pojo.Content;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 爬虫业务service
 *
 * @author jiajia
 * @date 2020/12/28
 */
public interface ContentService {

    /**
     * 将爬取结果放入到es中
     */
    Boolean parseContent(String keyword) throws Exception;

    /**
     * 查询结果返回
     * @param keyword
     * @param from
     * @param size
     * @return
     * @throws IOException
     */
    List<Map<String, Object>> getContent(String keyword, Integer from, Integer size) throws IOException;

    /**
     * 查询结果高亮返回
     * @param keyword
     * @param from
     * @param size
     * @return
     * @throws IOException
     */
    List<Map<String, Object>> getHighLightContent(String keyword, Integer from, Integer size) throws IOException;

}
