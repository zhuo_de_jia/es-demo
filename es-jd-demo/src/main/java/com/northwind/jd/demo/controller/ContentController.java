package com.northwind.jd.demo.controller;

import com.northwind.jd.demo.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * es调用 controller
 * @author jiajia
 * @date 2020/12/28
 */
@RestController
@RequestMapping("/es")
public class ContentController {

    @Autowired
    private ContentService contentService;

    /**
     * 解析
     * <p>
     * http://localhost:8081/es/parseContent/%E8%81%94%E6%83%B3
     *
     * @param keyword
     * @return
     */
    @GetMapping("/parseContent/{keyword}")
    public Boolean parseContent2ES(@PathVariable("keyword") String keyword) {
        try {
            final Boolean aBoolean = contentService.parseContent(keyword);
            return aBoolean;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 查询获取结果
     *
     * http://localhost:8081/es/getContent/%E6%83%A0%E6%99%AE/1/10
     * pageNo, pageSize
     * @param keyword 查询条件
     * @param from 从xx开始
     * @param size 多少条纪录
     * @return
     */
    @GetMapping("/getContent/{keyword}/{from}/{size}")
    public List<Map<String, Object>> searchContent(@PathVariable("keyword") String keyword,
                                                   @PathVariable("from") Integer from,
                                                   @PathVariable("size") Integer size) throws IOException {

        return contentService.getHighLightContent(keyword, from, size);
    }

}
