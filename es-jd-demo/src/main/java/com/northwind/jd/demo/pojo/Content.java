package com.northwind.jd.demo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 爬虫对应实体类
 *
 * @author jiajia
 * @date 2020/12/28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {

    /**
     * 图片
     */
    private String img;

    /**
     * 价格
     */
    private String price;

    /**
     * 地址
     */
    private String title;

}
