package com.northwind.jd.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * indexController
 *
 * @author jiajia
 * @date 2020/12/28
 */
@Controller
public class IndexController {

    /**
     * 首页
     * @return
     */
    @GetMapping({"/", "/index"})
    public String index() {
        return "index";
    }

}
