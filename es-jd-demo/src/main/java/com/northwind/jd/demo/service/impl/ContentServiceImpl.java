package com.northwind.jd.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.northwind.jd.demo.pojo.Content;
import com.northwind.jd.demo.service.ContentService;
import com.northwind.jd.demo.utils.HtmlParseUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 爬虫业务
 *
 * @author jiajia
 * @date 2020/12/28
 */
@Service
public class ContentServiceImpl implements ContentService {

    // 高版本es连接
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    // 1、 解析数据放到es中
    @Override
    public Boolean parseContent(String keyword) throws Exception {

        // 通过爬虫获取数据
        final List<Content> contents = HtmlParseUtils.parseJD(keyword);

        //把查询的数据放入es中
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout(TimeValue.timeValueSeconds(3L));

        // 将数据放到请求中
        for (int i = 0; i < contents.size(); i++) {
            bulkRequest.add(
                    new IndexRequest("jd_goods")
                            .source(JSON.toJSONString(contents.get(i)), XContentType.JSON).type("_doc"));
//                            .source(JSON.toJSONString(contents.get(i)), XContentType.JSON).type("_doc"));
        }

        // 执行请求
        final BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

        // 返回结果
        return  !bulk.hasFailures();


    }

    // 2、查询,获取数据
    @Override
    public List<Map<String, Object>> getContent(String keyword, Integer from, Integer size) throws IOException {

        //region 前置判断
        if (null == from || from <= 1) {
            from = 1;
        }

        if(null == size || size <= 1) {
            size = 1;
        }
        //endregion

        // 实现搜索功能

        // 条件搜索
        final SearchRequest searchRequest = new SearchRequest("jd_goods");

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        // 这里字段没有设置中文分词器，要设置上后，才能进行中文查询，否则最多为单个中文匹配
        // 精准匹配
        final TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", keyword);

        searchSourceBuilder.query(termQueryBuilder);

        // 模糊匹配
//        final MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("title.keyword", keyword);
//
//        searchSourceBuilder.query(matchQueryBuilder);

        // 设置超时
        searchSourceBuilder.timeout(TimeValue.timeValueSeconds(3L));
//        searchSourceBuilder.timeout(new TimeValue(30, TimeUnit.SECONDS));

        // 设置分页
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        // 执行搜索
        searchRequest.source(searchSourceBuilder);
        final SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        // 解析结果
        final SearchHit[] hits = searchResponse.getHits().getHits();

        final ArrayList<Map<String, Object>> list = new ArrayList<>();
        // 遍历插入结果
        for (SearchHit hit : hits) {
            final Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            list.add(sourceAsMap);
        }

        // 返回结果
        return list;
    }

    // 3、查询,获取数据,实现高亮搜索
    @Override
    public List<Map<String, Object>> getHighLightContent(String keyword, Integer from, Integer size) throws IOException {

        //region 前置判断
        if (null == from || from <= 1) {
            from = 1;
        }

        if(null == size || size <= 1) {
            size = 1;
        }
        //endregion

        // 实现搜索功能

        // 条件搜索
        final SearchRequest searchRequest = new SearchRequest("jd_goods");

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        // 这里字段没有设置中文分词器，要设置上后，才能进行中文查询，否则最多为单个中文匹配
        // 精准匹配
        final TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", keyword);

        searchSourceBuilder.query(termQueryBuilder);

        // 模糊匹配
//        final MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("title.keyword", keyword);
//
//        searchSourceBuilder.query(matchQueryBuilder);

        // 设置超时
        searchSourceBuilder.timeout(TimeValue.timeValueSeconds(3L));
//        searchSourceBuilder.timeout(new TimeValue(30, TimeUnit.SECONDS));

        // 设置分页
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);

        // 设置高亮 HighlightBuilder
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        // 是否要所有命中高亮|是否要多个高亮显示
        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.field("title");
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");
        searchSourceBuilder.highlighter(highlightBuilder);

        // 执行搜索
        searchRequest.source(searchSourceBuilder);
        final SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        // 解析结果
        final SearchHit[] hits = searchResponse.getHits().getHits();

        final ArrayList<Map<String, Object>> list = new ArrayList<>();
        // 遍历插入结果
        for (SearchHit hit : hits) {
            final Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            // 对结果进行处理，解析高亮字段
            final Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            final HighlightField hightLightTitle = highlightFields.get("title");

            // 置换高亮字段
            if (hightLightTitle != null) {
                final Text[] fragments = hightLightTitle.fragments();

                StringBuilder sb = new StringBuilder();

                for (Text fragment : fragments) {
                    sb.append(fragment);
                }
                // 替换高亮字段
                sourceAsMap.put("title", sb);
            }

            list.add(sourceAsMap);
        }

        // 返回结果
        return list;
    }

}
