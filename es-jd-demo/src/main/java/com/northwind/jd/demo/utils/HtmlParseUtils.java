package com.northwind.jd.demo.utils;

import com.northwind.jd.demo.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 爬虫工具类
 * 该类不需要注入，直接调用即可
 *
 * @author jiajia
 * @date 2020/12/28
 */
public class HtmlParseUtils {

    private static final String REQUEST_URL = "https://search.jd.com/Search?keyword=";

    public static List<Content> parseJD(String keyword) throws Exception {

        // 获取请求 https://search.jd.com/Search?keyword=java&enc=utf-8&suggest=1.his.0.0&wq=&pvid=30bf41c16f8c4088997b8011aab77d7f
        String url = REQUEST_URL + keyword;

        // 解析网页(Jsoup返回的Document就是Document对象) 这里的时间是毫秒，这里即为30秒
        final Document document = Jsoup.parse(new URL(url), 30000);

        // 所有在js使用的元素，都可以在这里使用
        final Element element = document.getElementById("J_goodsList");

        // 获取所有li元素
        final Elements elements = element.getElementsByTag("li");

        List<Content> goodsList = new ArrayList<>();

        // 获取元素中的内容，这里的el,就是每个li标签了
        for (Element el : elements) {

            // 获取图片
            final String src = el.getElementsByTag("img").eq(0).attr("data-lazy-img");

            // 获取价格
            final String price = el.getElementsByClass("p-price").eq(0).text();

            // 获取标题
            final String title = el.getElementsByClass("p-name").eq(0).text();

            final Content content = new Content(src, price, title);

            goodsList.add(content);

        }
        return goodsList;
    }


    public static void main(String[] args) throws Exception {
        final List<Content> result = parseJD("联想");

        result.forEach(System.out::println);

//        for (int i = 0; i < result.size(); i++) {
//            System.out.println(result.get(i).toString());
//        }
    }

}
