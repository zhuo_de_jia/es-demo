package com.northwind.jd.demo.utils;

import com.alibaba.fastjson.JSON;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 学习时未整理util
 *
 * @author jiajia
 * @date 2020/12/28
 */
public class HtmlParseUtilsTest {

    public static void main(String[] args) throws IOException {
        // 获取请求 https://search.jd.com/Search?keyword=java&enc=utf-8&suggest=1.his.0.0&wq=&pvid=30bf41c16f8c4088997b8011aab77d7f
        // 前提，需要联网
        String url = "https://search.jd.com/Search?keyword=java&enc=utf-8&suggest=1.his.0.0&wq=&pvid=30bf41c16f8c4088997b8011aab77d7f";

        // 解析网页(Jsoup返回的Document就是Document对象) 这里的时间是毫秒，这里即为30秒
        final Document document = Jsoup.parse(new URL(url), 30000);

        // 所有在js使用的元素，都可以在这里使用
        final Element element = document.getElementById("J_goodsList");

        // 获取所有li元素
        final Elements elements = element.getElementsByTag("li");
        // 获取元素中的内容，这里的el,就是每个li标签了
        for (Element el : elements) {
//            System.out.println(el.html());

            // 获取图片
            // 关于图片特别多的网站，所有图片都是延迟加载的，下面这种方式，是获取不到地址的
//            final String src = el.getElementsByTag("img").eq(0).attr("src");
//            System.out.println(src);

//            final String src = el.getElementsByClass("source-data-lazy-img").eq(0).attr("src");
            final String src = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            System.out.println(src);

            // 获取价格
            final String price = el.getElementsByClass("p-price").eq(0).text();
            System.out.println(price);

            final String title = el.getElementsByClass("p-name").eq(0).text();
            System.out.println(title);

//            final String data = el.data();
        }



//        System.out.println(JSON.toJSONString(element));
        //这里输出的为网页信息
//        System.out.println(element.html());


//        final Document document = Jsoup.parse(url);

    }

}
