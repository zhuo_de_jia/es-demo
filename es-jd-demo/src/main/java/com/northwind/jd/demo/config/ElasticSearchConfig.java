package com.northwind.jd.demo.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ES配置
 *
 * @author jiajia
 * @date 2020/12/28
 */
@Configuration
public class ElasticSearchConfig {

    /**
     * 注入 相当于
     * spring <bean id="restHighLevelClient" class = "RestHighLevelClient"></bean>
     *
     * @return
     */
    @Bean
    public RestHighLevelClient restHighLevelClient() {

        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("127.0.0.1", 9200, "http")
                        //这里可以注入集群，即多个es服务
                        /*new HttpHost("localhost", 9200, "http"))*/));
        return client;
    }


}
